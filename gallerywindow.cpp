#include "gallerywindow.h"
#include "ui_gallerywindow.h"

GalleryWindow::GalleryWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GalleryWindow)
{
    ui->setupUi(this);

    //  Form creation
    {
        QGridLayout *gridBox = new QGridLayout();
        QHBoxLayout *hBox = new QHBoxLayout();

        tableWidget = new QTableWidget();
        tableWidget->setColumnCount(3);
        QStringList headers;
        headers <<"Name" << "Time" << "Image Count";
        tableWidget->setHorizontalHeaderLabels(headers);
        tableWidget->verticalHeader()->setVisible(false);
        QHeaderView *header = tableWidget->horizontalHeader();
        header->setSectionResizeMode(QHeaderView::Stretch);
//        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridBox->addWidget(tableWidget,0,0);

        addButton = new QPushButton("+");
        editButton = new QPushButton("e");
        deleteButton = new QPushButton("-");
        quickButton = new QPushButton("Quick@");
        deepButton = new QPushButton("Deep@");

        hBox->addStretch();
        hBox->addWidget(addButton);
        hBox->addWidget(editButton);
        hBox->addWidget(deleteButton);
        hBox->addWidget(quickButton);
        hBox->addWidget(deepButton);
        hBox->addStretch();

        gridBox->addLayout(hBox,1,0);

        addButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        editButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        deleteButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));

        ui->centralWidget->setLayout(gridBox);
    }

    personWindow = new PersonWindow();

    indexer = &Indexer::getInstance();

    connect(addButton,SIGNAL(clicked()),this,SLOT(onAddClick()));
    connect(editButton,SIGNAL(clicked()),this,SLOT(onEditClick()));
    connect(deleteButton,SIGNAL(clicked()),this,SLOT(onDeleteClick()));
    connect(quickButton,SIGNAL(clicked()),this,SLOT(quickScan()));
    connect(deepButton,SIGNAL(clicked()),this,SLOT(deepScan()));

    connect(tableWidget,SIGNAL(itemDoubleClicked(QTableWidgetItem*)),SLOT(onDoubleClick(QTableWidgetItem*)));
    connect(personWindow,SIGNAL(updateInfo()),this,SLOT(drawGalleries()));

    drawGalleries();
}

GalleryWindow::~GalleryWindow()
{
    delete ui;
}

///  ==================== User Interface =================================

void GalleryWindow::closeEvent(QCloseEvent*)
{
    if(!personWindow->isHidden())
        personWindow->close();
    personWindow->deleteLater();
}

void GalleryWindow::onAddClick()
{
    personWindow->setWindowTitle("Capture new faces");
    personWindow->showCapture();
}

void GalleryWindow::onEditClick()
{
    if(tableWidget->selectedItems().empty())
        return;
    else{
//        curGallery = tableWidget->selectedItems()[0]->row();
        curGallery = galleryInfo[tableWidget->selectedItems()[0]->row()].galleryID;
        personWindow->showEdit(curGallery);
    }
}

void GalleryWindow::onDeleteClick()
{
    if(tableWidget->selectedItems().empty())
        return;
    else{
        curGallery = galleryInfo[tableWidget->selectedItems()[0]->row()].galleryID;
        DBGallery tGallery;
        tGallery.load(curGallery);
        tGallery.deleteAll();
    }
    drawGalleries();
}

void GalleryWindow::onDoubleClick(QTableWidgetItem *item)
{
    curGallery = item->row();
    personWindow->showEdit(curGallery);
}

void GalleryWindow::drawGalleries()
{
    galleryInfo = DBGallery::getGalleryInfo();

    tableWidget->setRowCount(galleryInfo.size());
    int i = 0;
    for(QList<DBGallery::GalleryInfo>::iterator it = galleryInfo.begin(); it < galleryInfo.end(); ++it, ++i){
        tableWidget->setItem(i,0,new QTableWidgetItem(it->personName));
        tableWidget->setItem(i,1,new QTableWidgetItem(it->galleryDate.toString(Qt::DefaultLocaleShortDate)));
        tableWidget->setItem(i,2,new QTableWidgetItem(QString::number(it->imageCount)));
    }

    tableWidget->clearSelection();
}

void GalleryWindow::quickScan()
{
    emit indexer->quickScan();
    drawGalleries();
}

void GalleryWindow::deepScan()
{
    emit indexer->deepScan();
    drawGalleries();
}
