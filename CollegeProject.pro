#-------------------------------------------------
#
# Project created by QtCreator 2015-03-18T12:19:57
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CollegeProject
TEMPLATE = app

LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

SOURCES += main.cpp\
        gallerywindow.cpp\
    personwindow.cpp

HEADERS  += gallerywindow.h\
    personwindow.h

FORMS    += gallerywindow.ui\
    personwindow.ui

include(Proc/Proc.pri)
#include(Hardware/Hardware.pri)
include(Struct/Struct.pri)
include(Widgets/Widgets.pri)
#include(Struct2/Struct2.pri)

DISTFILES +=

RESOURCES += \
    res.qrc
