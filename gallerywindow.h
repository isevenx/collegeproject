#ifndef GALLERYWINDOW_H
#define GALLERYWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QTableWidgetItem>

#include <QDebug>
#include <QDir>
#include <QFileInfoList>
#include <QStringList>

#include "indexer.h"
#include "personwindow.h"

namespace Ui {
class GalleryWindow;
}

class GalleryWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GalleryWindow(QWidget *parent = 0);
    ~GalleryWindow();

private:
    Ui::GalleryWindow *ui;
    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *deleteButton;
    QPushButton *quickButton;
    QPushButton *deepButton;
    QTableWidget *tableWidget;

    PersonWindow *personWindow;
    QList<DBGallery::GalleryInfo> galleryInfo;
    Indexer *indexer;

    QString path;
    qint64 curGallery;

    void closeEvent(QCloseEvent*);
private slots:
    void onAddClick();
    void onEditClick();
    void onDeleteClick();
    void onDoubleClick(QTableWidgetItem *item);
    void drawGalleries();
    void quickScan();
    void deepScan();
};

#endif // GALLERYWINDOW_H
