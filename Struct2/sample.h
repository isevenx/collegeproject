#ifndef SAMPLE_H
#define SAMPLE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <QList>

#include <deque>

#include "sampleposition.h"

class Sample
{
public:
    Sample();

    QString name;
    QString settings;

    int positionCount;
    int cells;
    int clusters;
    int artefacts;
    int processed;
    double result;
    double resultCells;
    double resultArtefacts;

    qint64 created;

    bool save();
    bool saveAll();

    bool load(qint64 dbID);
    bool loadAll(qint64 dbID);

    bool deleteAll();
    static bool deleteById(qint64 dbID);


    bool calcStats();

    inline qint64 getDbID(){return dbID;}
    static Sample get(qint64 dbID);
    static QList<Sample> getList();

    SamplePosition *getPositionById(qint64 dbID);

    SamplePosition::PositionInfo getPositionInfoById(qint64 dbID, int offset = 0, char filter = ' ');

    std::deque<SamplePosition> positions;

private:

    qint64 dbID;

};



#endif // SAMPLE_H
