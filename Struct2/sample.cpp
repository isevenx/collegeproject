#include "sample.h"

Sample::Sample()
{
    dbID = -1;
    name = QString::number(QDateTime::currentMSecsSinceEpoch());

    positionCount =0;
    cells =0;
    clusters=0;
    artefacts=0;
    processed=0;
    result=0;
    resultCells=0;
    resultArtefacts=0;
    created = QDateTime::currentMSecsSinceEpoch();
    settings = "";
}


bool Sample::save()
{

    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO samples (name,position_count,cells,clusters,artefacts,processed,result,created,result_cells,result_artefacts,settings) "
                      "VALUES (:name,:position_count,:cells,:clusters,:artefacts,:processed,:result,:created,:result_cells,:result_artefacts,:settings) ");
    }else{
        query.prepare("UPDATE samples SET "
                      "name = :name, "
                      "position_count = :position_count, "
                      "cells = :cells, "
                      "clusters = :clusters, "
                      "artefacts = :artefacts, "
                      "processed = :processed, "
                      "result = :result, "
                      "result_cells = :result_cells, "
                      "result_artefacts = :result_artefacts, "
                      "settings = :settings, "
                      "created = :created "
                      "WHERE id = :id"
                      );
        query.bindValue(":id", dbID);
    }
    query.bindValue(":name", name);
    query.bindValue(":position_count", positionCount);
    query.bindValue(":cells", cells);
    query.bindValue(":clusters", clusters);
    query.bindValue(":artefacts", artefacts);
    query.bindValue(":processed", processed);
    query.bindValue(":result", result);
    query.bindValue(":result_cells", resultCells);
    query.bindValue(":result_artefacts", resultArtefacts);
    query.bindValue(":settings", settings);
    query.bindValue(":created", created);

    if(!query.exec()){
        qDebug() << "Sample::save error - " << query.lastError().text() << query.executedQuery() ;
        return false;
    }
    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool Sample::saveAll()
{
    QSqlQuery query;
    bool result = false;

    query.exec("PRAGMA journal_mode = MEMORY");
    query.exec("PRAGMA synchronous = OFF");
    query.exec("BEGIN Transaction");

    if(save()) {

        for(std::deque<SamplePosition>::iterator it = positions.begin(); it != positions.end(); ++it){
            (*it).sampleId = dbID;
            (*it).saveAll();
        }
        result = true;
    }

    query.exec("End Transaction");
    query.exec("PRAGMA journal_mode = DELETE ");
    query.exec("PRAGMA synchronous = FULL");

    return result;
}


bool Sample::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM samples WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->name = query.value("name").toString();
        this->positionCount = query.value("position_count").toInt();
        this->cells = query.value("cells").toInt();
        this->clusters = query.value("clusters").toInt();
        this->artefacts = query.value("artefacts").toInt();
        this->processed = query.value("processed").toInt();
        this->result = query.value("result").toDouble();
        this->resultCells = query.value("result_cells").toDouble();
        this->resultArtefacts = query.value("result_artefacts").toDouble();
        this->settings = query.value("settings").toString();
        this->created = query.value("created").toInt();
        this->dbID = dbID;
        return true;
    }
    return false;
}

bool Sample::calcStats()
{
    QSqlQuery query;
    query.prepare("SELECT "\
                  "COUNT(id) AS positions, "\
                  "SUM(cells) AS cells, "\
                  "SUM(artefacts) AS artefacts, "\
                  "SUM(clusters) AS clusters, "\
                  "SUM(processed) AS processed "\
                  "FROM `sample_positions` WHERE sample_id = :id AND ignore = 0  AND processed = 1 GROUP BY  sample_id  ORDER BY `rowid` DESC LIMIT 0, 180;");

    query.bindValue(":id", dbID);

    if(query.exec() && query.first()){
        this->cells = query.value("cells").toInt();
        this->artefacts = query.value("artefacts").toInt();
        this->clusters = query.value("clusters").toInt();
        this->processed = query.value("processed").toInt();
        this->positionCount = query.value("positions").toInt();

        this->result = this->processed==0?0:(double)this->clusters/(double)this->processed*19040.;
        this->resultCells = this->processed==0?0:(double)this->cells/(double)this->processed*19040.;
        this->resultArtefacts = this->processed==0?0:(double)this->artefacts/(double)this->processed*19040.;
        qDebug() << "result - "<<this->result;

        return save();
    }

    qDebug() << "Sample::calcStats error - " << query.lastError().text() << query.executedQuery() ;
    return false;
}

QList<Sample> Sample::getList()
{
    QSqlQuery query;
    QList<Sample> result;
    query.prepare("SELECT * FROM samples ORDER BY id desc");
    if(!query.exec()){
        qDebug() << "Sample::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    Sample tSample;

    while (query.next()) {
        tSample.dbID = query.value("id").toInt();
        tSample.name = query.value("name").toString();
        tSample.positionCount = query.value("position_count").toInt();
        tSample.cells = query.value("cells").toInt();
        tSample.clusters = query.value("clusters").toInt();
        tSample.artefacts = query.value("artefacts").toInt();
        tSample.processed = query.value("processed").toInt();
        tSample.result = query.value("result").toDouble();
        tSample.resultCells = query.value("result_cells").toDouble();
        tSample.resultArtefacts = query.value("result_artefacts").toDouble();
        tSample.settings = query.value("settings").toString();
        tSample.created = query.value("created").toInt();

        //tSample.calcStats();
        result.push_back(tSample);
    }

    return result;
}

bool Sample::loadAll(qint64 dbID)
{
    bool result = false;
    if(load(dbID))
    {
        this->positions = SamplePosition::getList(dbID);
        result = true;
    }
    return result;
}

Sample Sample::get(qint64 dbID)
{
    Sample sample;
    sample.load(dbID);
    return sample;
}

SamplePosition * Sample::getPositionById(qint64 dbID)
{
    std::deque<SamplePosition>::iterator it = positions.begin();
    while( it != positions.end() && (*it).getDbID()  != dbID) ++it;
    if( it == positions.end()) return 0;
    return &(*it);
}

SamplePosition::PositionInfo Sample::getPositionInfoById(qint64 dbID, int offset, char filter)
{
    SamplePosition::PositionInfo result;
    result.found = false;

    std::deque<SamplePosition>::iterator it = positions.begin();
    if(dbID <= 0 && offset > 0){
        dbID = ((SamplePosition)positions.front()).getDbID();
        offset = 0;
    }else if(dbID <= 0 && offset < 0){
        dbID = ((SamplePosition)positions.back()).getDbID();
        offset = 0;
    }

    int i=0;
    while( it != positions.end() && (*it).getDbID()  != dbID){
        ++it;
        i++;
    }

    if( it == positions.end()) return result;
    qDebug() << filter;
    if( offset > 0){
        if(filter != ' '){
            while(it != positions.end()){
                it++;
                if(it == positions.end()) return result;
                i++;
                if(filter == 'c' && ((SamplePosition)(*it)).cells > 0) break;
                if(filter == 'a' && ((SamplePosition)(*it)).artefacts > 0) break;
                if(filter == 'i' && ((SamplePosition)(*it)).ignore) break;
                if(filter == 'p' && ((SamplePosition)(*it)).print) break;
            }
        }else{
            it++;
            if( it == positions.end()) return result;
            i++;
        }
    }
    if( offset < 0){
        if(filter != ' '){
            while(it != positions.begin()){
                it--;
                i--;
                if(filter == 'c' && ((SamplePosition)(*it)).cells > 0) break;
                if(filter == 'a' && ((SamplePosition)(*it)).artefacts > 0) break;
                if(filter == 'i' && ((SamplePosition)(*it)).ignore) break;
                if(filter == 'p' && ((SamplePosition)(*it)).print) break;
            }
            if(it == positions.begin()){
                if(!(   (filter == 'p' && ((SamplePosition)(*it)).print)||
                        (filter == 'i' && ((SamplePosition)(*it)).ignore)||
                        (filter == 'c' && ((SamplePosition)(*it)).cells > 0) ||
                        (filter == 'a' && ((SamplePosition)(*it)).artefacts > 0)
                        ))
                    return result;
            }
        }else{
            if(it == positions.begin()) return result;
            it--;
            i--;
        }
    }

    result.position = &(*it);
    result.totalPositions = positions.size();
    result.nr = i;

    result.found = true;
    return result;
}

bool Sample::deleteAll()
{
    return Sample::deleteById(dbID);
}

bool Sample::deleteById(qint64 dbID)
{
    if(dbID < 1) return false;

    QSqlQuery query;
    query.prepare("SELECT id FROM sample_positions WHERE sample_id = :id ");
    query.bindValue(":id", dbID);
    if(!query.exec()){
        qDebug() << "Sample::deleteById error - " << query.lastError().text() << query.executedQuery() ;
        return false;
    }

    QDir tDir;
    while (query.next()) {
        tDir = QDir(SampleImage::imageAbsoluteDir(query.value("id").toInt()));
        if(tDir.exists()) tDir.removeRecursively();
    }

    QSqlQuery query2;
    query2.prepare("DELETE FROM images WHERE sample_position_id in ( SELECT id FROM sample_positions WHERE sample_id = :id)");
    query2.bindValue(":id", dbID);
    if(!query2.exec()){
        qDebug() << "Sample::deleteById error - " << query2.lastError().text() << query2.executedQuery() ;
        return false;
    }

    QSqlQuery query3;
    query3.prepare("DELETE FROM sample_positions WHERE sample_id = :id");
    query3.bindValue(":id", dbID);
    if(!query3.exec()){
        qDebug() << "Sample::deleteById error - " << query3.lastError().text() << query3.executedQuery() ;
        return false;
    }

    QSqlQuery query4;
    query4.prepare("DELETE FROM samples WHERE id = :id");
    query4.bindValue(":id", dbID);
    if(!query4.exec()){
        qDebug() << "Sample::deleteById error - " << query4.lastError().text() << query4.executedQuery() ;
        return false;
    }

    return true;
}
