#ifndef SAMPLEIMAGE_H
#define SAMPLEIMAGE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <QDateTime>
#include <QDir>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


class SampleImage
{
public:
    static QDir imageDir;

    typedef enum{
        GREEN = 0,
        BLUE = 1,
        RED = 2,
        TMP = 3,
        GREEN_INFO = 4,
        BLUE_INFO = 5,
    } SampleImageType;

    SampleImage();

    SampleImageType type;
    int samplePositionId;
    // Used for info images
    qint64 parentImageId;
    double focus;

    cv::Mat getImg();
    void setImg(cv::Mat img);

    bool save();
    bool saveAll();

    bool load(qint64 dbID);
    bool loadAll(qint64 dbID);

    inline qint64 getDbID(){return dbID;}
    static SampleImage get(qint64 dbID);

    static QFileInfo getFilePath(qint64 positionId, qint64 dbID, bool thumbnail = true);
    static std::vector<SampleImage> getInfoList(qint64 samplePositionDbID, SampleImageType imageType);

    static QString imageFolder(qint64 id);
    static QString imageAbsoluteDir(qint64 positionId);
    static QString imageAbsoluteDirOld(qint64 positionId);

    static int typeToInt(SampleImageType type);
    static SampleImageType intToType(int type);

private:

    qint64 dbID;
    cv::Mat img;
};

#endif // SAMPLEIMAGE_H
