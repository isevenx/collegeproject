#ifndef SAMPLEPOSITION_H
#define SAMPLEPOSITION_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include <QVariant>
#include <QDateTime>

#include <vector>

#include "sampleimage.h"

class SamplePosition
{
public:
    SamplePosition();

    typedef struct {
        bool found;
        SamplePosition * position;
        int nr;
        int totalPositions;
    } PositionInfo;

    typedef enum {
        Artefact = 0,
        Cell = 1
    } CellType;

    typedef struct {
        cv::Mat img;
        int x;
        int y;
        int group;
        CellType cellType;
        std::map<QString,QString> data;
    } SampleObject;

    int sampleId;
    int gridX;
    int gridY;
    double x;
    double y;
    double width;
    double height;
    int bgImgCount;
    int fgImgCount;
    int blueImgCount;

    int greenInfoImgCount;
    int blueInfoImgCount;

    int cells;
    int clusters;
    int artefacts;
    bool processed;
    bool ignore;
    bool verified;
    bool edited;
    bool print;

    // Temporary
    bool visited;
    double backgroundFocus;
    long backgroundFocusValue;
    bool hasBackgroundFocus;
    bool hasForegroundFocus;
    bool checkedBlueFrame;
    bool hasBlueFrame;
    bool pickedForSpiral;

    std::vector<SampleImage> blueImages;
    std::vector<SampleImage> greenImages;
    std::vector<SampleImage> redImages;

    std::vector<SampleImage> greenInfoImages;
    std::vector<SampleImage> blueInfoImages;

    // For later processing
    std::vector<SampleImage> tmpImages;

    bool save();
    bool saveAll();
    bool load(qint64 dbID);
    bool loadImageInfo();

    inline qint64 getDbID(){return dbID;}
    static SamplePosition get(qint64 dbID);
    static std::deque<SamplePosition> getList(qint64 sampleDbID);

private:
    qint64 dbID;
};

#endif // SAMPLEPOSITION_H
