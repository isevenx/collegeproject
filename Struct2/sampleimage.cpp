#include "sampleimage.h"
#include "protsettings.h"

QDir SampleImage::imageDir(QDir::homePath().append(QString("/BactStore/DbTree/")));

SampleImage::SampleImage()
{
    dbID = -1;

    type = GREEN;
    samplePositionId = 0;
    parentImageId = 0;
    focus = 0;
}


QString SampleImage::imageFolder(qint64 id)
{
    QString idSTR = QString::number(id).rightJustified(10,'0');
    return QString("%1/%2/%3").arg( idSTR.mid(0,3)).arg(idSTR.mid(3,3)).arg(idSTR.mid(6));
}

QString SampleImage::imageAbsoluteDir(qint64 positionId)
{
    return imageDir.absolutePath().append("/%1").arg(imageFolder(positionId));
}

QString SampleImage::imageAbsoluteDirOld(qint64 positionId)
{
    return QDir::homePath().append(QString("/BactStore/Db/%1").arg(QString::number(positionId)));
}

bool SampleImage::save()
{
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO images (type, sample_position_id, focus, parent_image_id) "
                      "VALUES (:type, :sample_position_id, :focus, :parent_image_id)");
    }else{
        query.prepare("UPDATE images SET "
                      "type = :type, "
                      "sample_position_id = :sample_position_id, "
                      "focus = :focus, "
                      "parent_image_id = :parent_image_id "
                      "WHERE id = :id"
                      );
        query.bindValue(":id", dbID);
    }

    query.bindValue(":type", typeToInt(type));
    query.bindValue(":sample_position_id", samplePositionId);
    query.bindValue(":focus", focus);
    query.bindValue(":parent_image_id", parentImageId);

    if(!query.exec()){
        qDebug() << query.lastError().text() << ": "<< query.executedQuery();
        return false;
    }
    if(dbID == -1) dbID = query.lastInsertId().toInt();

    if(!img.empty())
    {
        QDir initial(imageDir.absolutePath().append("/%1").arg(imageFolder(samplePositionId)));
        if(!initial.mkpath(".")){
            return false;
        }

        QFileInfo file(initial.filePath(QString("%1.jpg").arg(QString::number(dbID))));
        QFileInfo tfile(initial.filePath(QString("t%1.jpg").arg(QString::number(dbID))));

        if(file.exists() && tfile.exists()){
            return false;
        }

        std::vector<int> params;
        params.push_back(cv::IMWRITE_JPEG_QUALITY);

        if(type == GREEN_INFO || type == BLUE_INFO){
            params.push_back(87);
        }else{
            params.push_back(100);
        }

        cv::imwrite( file.absoluteFilePath().toStdString().c_str(), img,params);

        cv::Mat thumb;
        cv::Size tSize = img.size();
        tSize.width = 200;
        tSize.height = (double)tSize.width/img.size().width*img.size().height;

        cv::resize(img,thumb,tSize,0,cv::INTER_AREA );

        std::vector<int> tparams;
        tparams.push_back(cv::IMWRITE_JPEG_QUALITY);
        tparams.push_back(76);

        cv::imwrite( tfile.absoluteFilePath().toStdString().c_str(), thumb,tparams);
        qDebug() << "Saving thumb" << tfile.absoluteFilePath();

        img = cv::Mat();
    }

    return true;
}

bool SampleImage::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->type = intToType(query.value("type").toInt());
        this->samplePositionId = query.value("sample_position_id").toInt();
        this->focus = query.value("focus").toDouble();
        this->parentImageId = query.value("parent_image_id").toInt();
        this->dbID = dbID;


        QDir initial(imageDir.absolutePath().append("/%1").arg(imageFolder(samplePositionId)));
        if(!initial.mkpath(".")){
            return false;
        }

        QFileInfo file(initial.filePath(QString::number(dbID).append(".jpg")));
        if(!file.exists()){
            initial = QDir(QDir::homePath().append(QString("/BactStore/Db/%1").arg(QString::number(samplePositionId))));
            file = QFileInfo(initial.filePath(QString::number(dbID).append(".jpg")));

            if(!file.exists()){
                return false;
            }
        }
        img = cv::imread(file.absoluteFilePath().toStdString().c_str());
        return true;
    }
    return false;
}

SampleImage SampleImage::get(qint64 dbID)
{
    SampleImage sample;
    sample.load(dbID);
    return sample;
}

cv::Mat SampleImage::getImg()
{
    if(img.empty() && dbID > 0 && samplePositionId > 0)
    {
        QDir initial(imageDir.absolutePath().append("/%1").arg(imageFolder(samplePositionId)));
        if(!initial.mkpath(".")){
            return img;
        }

        QFileInfo file(initial.filePath(QString::number(dbID).append(".jpg")));
        if(!file.exists()){
            QDir initialOld(QDir::homePath().append(QString("/BactStore/Db/%1").arg(QString::number(samplePositionId))));
            QFileInfo fileOld(initialOld.filePath(QString::number(dbID).append(".jpg")));
            if(fileOld.exists()){
                QDir tDir;
                tDir.rename(fileOld.absoluteFilePath(),file.absoluteFilePath());
            }
        }

        cv::Mat tImg = cv::imread(file.absoluteFilePath().toLocal8Bit().constData());
        return tImg;
    }
    return img;
}


QFileInfo SampleImage::getFilePath(qint64 positionId, qint64 dbID, bool thumbnail)
{
    QDir initial(imageDir.absolutePath().append("/%1").arg(imageFolder(positionId)));

    if(!initial.mkpath(".")){
        return QFileInfo();
    }

    QFileInfo file(initial.filePath(QString("%1%2.jpg").arg(thumbnail?"t":"").arg(dbID)));
    qDebug() << "Thumb requested" << thumbnail << " " << file.absoluteFilePath();

    if(!file.exists()){
        //TODO useless for production
        qDebug() << "Thumb not found";
        SampleImage tImg;
        tImg.load(dbID);
        tImg.save();
    }

    return file;
}

std::vector<SampleImage> SampleImage::getInfoList(qint64 samplePositionDbID, SampleImageType imageType)
{
    QSqlQuery query;
    std::vector<SampleImage> result;
    if(imageType >= 4){
        query.prepare("SELECT * FROM images WHERE sample_position_id = :sample_position_id AND type = :type ORDER BY focus ASC");
    }else{
        // For backwards compatability
        query.prepare("SELECT * FROM images WHERE sample_position_id = :sample_position_id AND type = :type");
    }
    query.bindValue(":sample_position_id", samplePositionDbID);
    query.bindValue(":type", typeToInt(imageType));

    if(!query.exec()){
        qDebug() << "SampleImage::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    SampleImage tSampleImage;

    while (query.next()) {
        tSampleImage.dbID = query.value("id").toInt();
        tSampleImage.samplePositionId = query.value("sample_position_id").toInt();
        tSampleImage.type = intToType(query.value("type").toInt());
        tSampleImage.focus = query.value("focus").toDouble();
        tSampleImage.parentImageId = query.value("parent_image_id").toInt();
        result.push_back(tSampleImage);
    }

    return result;
}


void SampleImage::setImg(cv::Mat img)
{
    this->img = img;
}

int SampleImage::typeToInt(SampleImageType type)
{
    switch(type){
        case GREEN: return 0; break;
        case BLUE: return 1; break;
        case RED: return 2; break;
        case GREEN_INFO: return 4; break;
        case BLUE_INFO: return 5; break;
    }
    return 0;
}

SampleImage::SampleImageType SampleImage::intToType(int type)
{
    switch(type){
        case 0: return GREEN; break;
        case 1: return BLUE; break;
        case 2: return RED; break;
        case 4: return GREEN_INFO; break;
        case 5: return BLUE_INFO; break;
    }
    return GREEN;
}
