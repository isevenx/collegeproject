#include "sampleposition.h"

SamplePosition::SamplePosition()
{
    dbID = -1;

    sampleId = -1;
    gridX = 0;
    gridY = 0;
    x = 0;
    y = 0;
    width = 0;
    height = 0;
    bgImgCount = 0;
    fgImgCount = 0;
    blueImgCount = 0;
    greenInfoImgCount = 0;
    blueInfoImgCount = 0;

    cells = 0;
    clusters = 0;
    artefacts = 0;
    processed = false;
    ignore = false;
    verified = false;
    edited = false;
    print = false;

    visited = false;
    backgroundFocus = 0;
    backgroundFocusValue = 0;
    hasBackgroundFocus = false;
    hasForegroundFocus = false;
    checkedBlueFrame;
    hasBlueFrame = false;
    pickedForSpiral = false;
}

bool SamplePosition::save()
{
    if(sampleId == -1) return false;
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO sample_positions (sample_id,gridx,gridy,x,y,width,height,bg_img_count,fg_img_count,blue_img_count,"
                      "green_info_img_count,blue_info_img_count,cells,clusters,artefacts,processed,ignore,verified,edited,print) "
                      "VALUES (:sample_id,:gridx,:gridy,:x,:y,:width,:height,:bg_img_count,:fg_img_count,:blue_img_count,"
                      ":green_info_img_count,:blue_info_img_count,:cells,:clusters,:artefacts,:processed,:ignore,:verified,:edited,:print)");
    }else{
        query.prepare("UPDATE sample_positions SET "
                      "sample_id = :sample_id, "
                      "gridx = :gridx ,"
                      "gridy = :gridy, "
                      "x = :x, "
                      "y = :y, "
                      "width = :width, "
                      "height = :height, "
                      "bg_img_count = :bg_img_count, "
                      "fg_img_count = :fg_img_count, "
                      "blue_img_count = :blue_img_count, "
                      "green_info_img_count = :green_info_img_count, "
                      "blue_info_img_count = :blue_info_img_count, "
                      "cells = :cells, "
                      "clusters = :clusters, "
                      "artefacts = :artefacts, "
                      "processed = :processed, "
                      "ignore = :ignore, "
                      "verified = :verified, "
                      "edited = :edited, "
                      "print = :print "
                      "WHERE id = :id"
                      );

        query.bindValue(":id", dbID);

    }
    query.bindValue(":sample_id", sampleId);
    query.bindValue(":gridx", gridX);
    query.bindValue(":gridy", gridY);
    query.bindValue(":x", x);
    query.bindValue(":y", y);
    query.bindValue(":width", width);
    query.bindValue(":height", height);
    query.bindValue(":bg_img_count", bgImgCount);
    query.bindValue(":fg_img_count", fgImgCount);
    query.bindValue(":blue_img_count", blueImgCount);
    query.bindValue(":green_info_img_count", greenInfoImgCount);
    query.bindValue(":blue_info_img_count", blueInfoImgCount);
    query.bindValue(":cells", cells);
    query.bindValue(":clusters", clusters);
    query.bindValue(":artefacts", artefacts);
    query.bindValue(":processed", processed?1:0);
    query.bindValue(":ignore", ignore?1:0);
    query.bindValue(":verified", verified?1:0);
    query.bindValue(":edited", edited?1:0);
    query.bindValue(":print", print?1:0);

    if(!query.exec()){
        qDebug() << " SQL Err: " << query.lastError().text() << " | " << query.executedQuery();
        return false;
    }
    if(dbID == -1) dbID = query.lastInsertId().toInt();

    return true;
}



bool SamplePosition::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM sample_positions WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->sampleId = query.value("sample_id").toInt();
        this->gridX = query.value("gridx").toInt();
        this->gridY = query.value("gridy").toInt();
        this->x = query.value("x").toDouble();
        this->y = query.value("y").toDouble();
        this->width = query.value("width").toDouble();
        this->height = query.value("height").toDouble();
        this->bgImgCount = query.value("bg_img_count").toInt();
        this->fgImgCount = query.value("fg_img_count").toInt();
        this->blueImgCount = query.value("blue_img_count").toInt();
        this->greenInfoImgCount = query.value("green_info_img_count").toInt();
        this->blueInfoImgCount = query.value("blue_info_img_count").toInt();
        this->cells = query.value("cells").toInt();
        this->clusters = query.value("clusters").toInt();
        this->artefacts = query.value("artefacts").toInt();
        this->processed = query.value("processed").toInt()>0;
        this->ignore = query.value("ignore").toInt()>0;
        this->verified = query.value("verified").toInt()>0;
        this->edited = query.value("edited").toInt()>0;
        this->print = query.value("print").toInt()>0;
        this->dbID = dbID;
        return true;
    }
    return false;
}


bool SamplePosition::saveAll()
{
    if(!save()) return false;

    for(std::vector<SampleImage>::iterator it = blueImages.begin(); it != blueImages.end(); ++it){
        (*it).samplePositionId = dbID;
        (*it).type = SampleImage::BLUE;
        (*it).save();
    }

    for(std::vector<SampleImage>::iterator it = greenImages.begin(); it != greenImages.end(); ++it){
        (*it).samplePositionId = dbID;
        (*it).type = SampleImage::GREEN;
        (*it).save();
    }

    for(std::vector<SampleImage>::iterator it = redImages.begin(); it != redImages.end(); ++it){
        (*it).samplePositionId = dbID;
        (*it).type = SampleImage::RED;
        (*it).save();
    }
}

std::deque<SamplePosition> SamplePosition::getList(qint64 sampleDbID)
{
    QSqlQuery query;
    std::deque<SamplePosition> result;
    query.prepare("SELECT * FROM sample_positions WHERE sample_id = :sample_id ");
    query.bindValue(":sample_id", sampleDbID);

    if(!query.exec()){
        qDebug() << "SamplePosition::getList error - " << query.lastError().text() << query.executedQuery() ;
        return result;
    }

    SamplePosition tSamplePosition;

    while (query.next()) {
        tSamplePosition.dbID = query.value("id").toInt();
        tSamplePosition.sampleId = query.value("sample_id").toInt();
        tSamplePosition.gridX = query.value("gridx").toInt();
        tSamplePosition.gridY = query.value("gridy").toInt();
        tSamplePosition.x = query.value("x").toInt();
        tSamplePosition.y = query.value("y").toInt();
        tSamplePosition.width = query.value("width").toInt();
        tSamplePosition.height = query.value("height").toInt();
        tSamplePosition.bgImgCount = query.value("bg_img_count").toInt();
        tSamplePosition.fgImgCount = query.value("fg_img_count").toInt();
        tSamplePosition.blueImgCount = query.value("blue_img_count").toInt();        
        tSamplePosition.greenInfoImgCount = query.value("green_info_img_count").toInt();
        tSamplePosition.blueInfoImgCount = query.value("blue_info_img_count").toInt();
        tSamplePosition.cells = query.value("cells").toInt();
        tSamplePosition.clusters = query.value("clusters").toInt();
        tSamplePosition.artefacts = query.value("artefacts").toInt();
        tSamplePosition.processed = query.value("processed").toInt();
        tSamplePosition.ignore = query.value("ignore").toInt();
        tSamplePosition.verified = query.value("verified").toInt();
        tSamplePosition.edited = query.value("edited").toInt();
        tSamplePosition.print = query.value("print").toInt();
        result.push_back(tSamplePosition);
    }

    return result;
}


bool SamplePosition::loadImageInfo()
{
    bool result = false;
    if(dbID <= 0) return result;

    greenImages = SampleImage::getInfoList(dbID,SampleImage::GREEN);
    blueImages = SampleImage::getInfoList(dbID,SampleImage::BLUE);
    redImages = SampleImage::getInfoList(dbID,SampleImage::RED);

    greenInfoImages = SampleImage::getInfoList(dbID,SampleImage::GREEN_INFO);
    blueInfoImages = SampleImage::getInfoList(dbID,SampleImage::BLUE_INFO);
}

SamplePosition SamplePosition::get(qint64 dbID)
{
    SamplePosition samplePosition;
    samplePosition.load(dbID);
    return samplePosition;
}
