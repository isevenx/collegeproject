#include "sampledb.h"

const int SampleDb::versionRequired = 7;

SampleDb::SampleDb(QObject *parent) :
    QObject(parent)
{
    db = QSqlDatabase::addDatabase("QSQLITE");

    QDir initial(QDir::homePath().append("/BactStore/"));
    if(!initial.mkpath(".")){
        initial = QDir::home();
    }

    db.setDatabaseName( initial.filePath(settings.value("sample_db_filename","samples.db").toString()));
    ok = db.open();

    if(ok) ok = checkTables();

    if(ok){
        int currVerion = checkDbVersion();
        qDebug() << "Got DB version" << currVerion;
        if(currVerion < versionRequired){
            ok = applyPatches(currVerion);
        }
    }
}


bool SampleDb::checkTables()
{
    QSqlQuery q;
    q = db.exec(
                "CREATE TABLE IF NOT EXISTS samples(" \
                "id                 INTEGER, " \
                "name               TEXT DEFAULT \"\", "\
                "position_count     INTEGER  DEFAULT 0, " \
                "cells              INTEGER  DEFAULT 0, " \
                "clusters           INTEGER  DEFAULT 0, " \
                "artefacts          INTEGER  DEFAULT 0, " \
                "processed          INTEGER  DEFAULT 0, " \
                "result             REAL  DEFAULT 0, " \
                "created          INTEGER  DEFAULT 0, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS sample_positions(" \
                "id INTEGER, " \
                "sample_id INTEGER REFERENCES samples(id), " \
                "gridx          INTEGER  DEFAULT 0, " \
                "gridy          INTEGER  DEFAULT 0, " \
                "x              REAL  DEFAULT 0, " \
                "y              REAL  DEFAULT 0, " \
                "width          REAL  DEFAULT 0, " \
                "height         REAL  DEFAULT 0, " \
                "bg_img_count   INTEGER  DEFAULT 0, " \
                "fg_img_count   INTEGER  DEFAULT 0, " \
                "blue_img_count INTEGER  DEFAULT 0, " \
                "cells          INTEGER  DEFAULT 0, " \
                "clusters       INTEGER  DEFAULT 0, " \
                "artefacts      INTEGER  DEFAULT 0, " \
                "ignore         INTEGER  DEFAULT 0, " \
                "verified       INTEGER  DEFAULT 0, " \
                "edited         INTEGER  DEFAULT 0, " \
                "processed      INTEGER  DEFAULT 0, " \
                "print      INTEGER  DEFAULT 0, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS images(" \
                "id                 INTEGER, " \
                "type               INTEGER  DEFAULT 0, " \
                "sample_position_id INTEGER  DEFAULT 0 REFERENCES sample_positions(id), " \
                "focus              REAL  DEFAULT 0, " \
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    return true;
}

int SampleDb::checkDbVersion()
{
    int version = 0;

    QSqlQuery q;
    q = db.exec("SELECT name FROM sqlite_master WHERE type='table' AND name='dbversionlog';");
    if(!q.first()) return version;
    if(q.value("name").toString() != "dbversionlog") return version;

    q = db.exec("SELECT MAX(version) as version FROM dbversionlog;");
    if(!q.first()) return version;
    bool ok;
    version = q.value("version").toInt(&ok);
    if(!ok) return 0;

    return version;
}

bool SampleDb::applyPatches(int currentVerion)
{
    QSqlQuery q;
    if(currentVerion < 1){
        q = db.exec(
                    "CREATE TABLE IF NOT EXISTS dbversionlog(" \
                    "id                 INTEGER, " \
                    "version            INTEGER  DEFAULT 0, " \
                    "sqltime TIMESTAMP  DEFAULT CURRENT_TIMESTAMP NOT NULL, " \
                    "PRIMARY KEY(id ASC));");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(1)) return false;
    }
    if(currentVerion < 2){
        q = db.exec(
                    "ALTER TABLE samples ADD " \
                    "result_cells    REAL  DEFAULT 0" \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(2)) return false;
    }
    if(currentVerion < 3){
        q = db.exec(
                    "ALTER TABLE samples ADD " \
                    "result_artefacts   REAL  DEFAULT 0" \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(3)) return false;
    }
    if(currentVerion < 4){
        q = db.exec(
                    "ALTER TABLE samples ADD " \
                    "settings             TEXT DEFAULT \"\" " \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(4)) return false;
    }
    if(currentVerion < 5){
        q = db.exec(
                    "ALTER TABLE sample_positions ADD " \
                    "green_info_img_count INTEGER  DEFAULT 0 " \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(5)) return false;
    }
    if(currentVerion < 6){
        q = db.exec(
                    "ALTER TABLE sample_positions ADD " \
                    "blue_info_img_count INTEGER  DEFAULT 0 " \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(6)) return false;
    }
    if(currentVerion < 7){
        q = db.exec(
                    "ALTER TABLE images ADD " \
                    "parent_image_id INTEGER  DEFAULT 0 " \
                    ";");
        if(!q.isActive ()){
            qDebug() << q.lastError().text();
            return false;
        }
        if(!insertNewVerionRow(7)) return false;
    }

    // Sample for 4 migration (also increase versionRequired)
//    if(currentVerion < 4){
//        q = db.exec(
//                    "CREATE TABLE IF NOT EXISTS dbversionlog(" \
//                    "id                 INTEGER, " \
//                    "version            INTEGER  DEFAULT 0, " \
//                    "sqltime TIMESTAMP  DEFAULT CURRENT_TIMESTAMP NOT NULL, " \
//                    "PRIMARY KEY(id ASC));");
//        if(!q.isActive ()){
//            qDebug() << q.lastError().text();
//            return false;
//        }
//        if(!insertNewVerionRow(4)) return false;
//    }
    return true;
}


bool SampleDb::insertNewVerionRow(int version)
{
    QSqlQuery q;
    q.prepare("INSERT INTO dbversionlog (version) "
                  "VALUES (:version)");
    q.bindValue(":version", version);
    if(!q.exec()){
        qDebug() << q.lastError().text() << ": "<< q.executedQuery();
        return false;
    }
}


