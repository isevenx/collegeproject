#ifndef SAMPLEDB_H
#define SAMPLEDB_H

#include <QObject>
#include <QSql>
#include <QDir>
#include <QSqlDatabase>
#include <QSettings>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

class SampleDb : public QObject
{
    Q_OBJECT
public:
    explicit SampleDb(QObject *parent = 0);

    inline bool isOk(){return ok;}

signals:

public slots:

private:
    QSqlDatabase db;
    QSettings settings;
    bool ok;

    bool checkTables();

    static const int versionRequired;
    int checkDbVersion();
    bool applyPatches(int currentVerion);
    bool insertNewVerionRow(int version);
};

#endif // SAMPLEDB_H
