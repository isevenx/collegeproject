INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/sampleposition.h \
    $$PWD/sampledb.h \
    $$PWD/sample.h \
    $$PWD/sampleimage.h \

SOURCES += \
    $$PWD/sampleposition.cpp \
    $$PWD/sampledb.cpp \
    $$PWD/sample.cpp \
    $$PWD/sampleimage.cpp

