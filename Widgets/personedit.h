#ifndef PERSONEDIT_H
#define PERSONEDIT_H

#include <QWidget>
#include <QTabWidget>
#include <QLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QScrollArea>
#include <QPainter>
#include <QComboBox>
#include <QCompleter>

#include <QDebug>
#include <QList>
#include <QPixmap>
#include <QDir>
#include <QFile>
#include <QDateTime>
#include <QStringList>
#include <QFileInfoList>
#include <QShortcut>
#include <QThread>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <QSettings>
#include <QMessageBox>

#include "flowlayout.h"
#include "clicklabel.h"
#include "indexer.h"

namespace Ui {
class PersonEdit;
}

class PersonEdit : public QWidget
{
    Q_OBJECT

public:
    explicit PersonEdit(QWidget *parent = 0);
    ~PersonEdit();

public slots:
    void loadImages(qint64 curGallery);
    void clearSelection();
    inline void reloadImages(){ loadImages(curGallery); }

signals:
    void closeEdit();

private:
    Ui::PersonEdit *ui;
    QTabWidget *tabWidget;    
    QScrollArea *scrollArea;
    FlowLayout *imageBox;
    QWidget *imageWidget;
    QList<ClickLabel*> imageLabels;
    QLabel *iLabel;
    QFormLayout *imageInfo;
    QLabel *fileName, *imageSize, *lastModified;
    QPushButton *deleteButton;

    QComboBox *personEdit, *labelEdit;
    QLineEdit *dateEdit;
    QPushButton *labelButton;
    FlowLayout *labelBox;
    QWidget *labelWidget;
    QList<ClickLabel*> galleryLabels;

    QPushButton *saveButton;

    QSettings settings;

    DBGallery gallery;
    Indexer *indexer;
    QMessageBox message;

    QFileInfoList imageList;
    QString path;
    int curImage;
    qint64 curGallery;

    QShortcut *delKey;

    int width, height;
    bool ifEqualize, ifSaved;
    static const double DELTA_PRIMARY = 0.7;

    void closeEvent(QCloseEvent *);
    QImage toQImage(Mat inMat);
    Mat equalize(Mat inMat);

private slots:
    void onImageClick(int ID);
    void onLabelClick(int ID);
    void onDeleteClick();
    void onSaveClick();
    void onAddClick();
};

#endif // PERSONEDIT_H
