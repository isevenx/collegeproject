#include "personedit.h"
#include "ui_personedit.h"

PersonEdit::PersonEdit(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PersonEdit)
{
    ui->setupUi(this);

    //  Form creation
    {
        QVBoxLayout *layout = new QVBoxLayout();
        tabWidget = new QTabWidget();

        //     First Tab
        {
            QVBoxLayout *vBox = new QVBoxLayout();
            imageWidget = new QWidget();
            imageBox = new FlowLayout();
            scrollArea = new QScrollArea();

            imageWidget->setLayout(imageBox);
            scrollArea->setWidget(imageWidget);
            scrollArea->setWidgetResizable(true);
            vBox->addWidget(scrollArea);

            QHBoxLayout *hBox = new QHBoxLayout();
            iLabel = new QLabel();
            iLabel->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
            hBox->addWidget(iLabel);

            imageInfo = new QFormLayout();
            fileName = new QLabel();
            imageSize = new QLabel();
            lastModified = new QLabel();
            imageInfo->addRow("File Name: ",fileName);
            imageInfo->addRow("Image size: ",imageSize);
            imageInfo->addRow("LastModified: ",lastModified);

            deleteButton = new QPushButton("Delete");
            deleteButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
            connect(deleteButton,SIGNAL(clicked()),this,SLOT(onDeleteClick()));            

            delKey = new QShortcut(QKeySequence("Delete"),this);
            connect(delKey,SIGNAL(activated()),this,SLOT(onDeleteClick()));

            imageInfo->addRow("",deleteButton);

            hBox->addLayout(imageInfo);

            vBox->addLayout(hBox);
            QWidget *tWidget_1 = new QWidget();
            tWidget_1->setLayout(vBox);
            tabWidget->addTab(tWidget_1,"Images");
        }
        //     Second tab
        {
            QVBoxLayout *vBox = new QVBoxLayout();
            QWidget *tWidget = new QWidget();
            QFormLayout *formBox = new QFormLayout();

            personEdit = new QComboBox();
            personEdit->setEditable(true);
            formBox->addRow(tr("Person name:"),personEdit);
            dateEdit = new QLineEdit();
            formBox->addRow(tr("Date:"),dateEdit);
            labelEdit = new QComboBox();
            labelEdit->setEditable(true);
            labelButton = new QPushButton("Add");
            labelButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
            connect(labelButton,SIGNAL(clicked()),this,SLOT(onAddClick()));            
            QHBoxLayout *hBox = new QHBoxLayout();
            hBox->addWidget(labelEdit);
            hBox->addWidget(labelButton);
            formBox->addRow(tr("Labels:"),hBox);
            tWidget->setLayout(formBox);
            tWidget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed));
            vBox->addWidget(tWidget);

            labelBox = new FlowLayout();
            labelWidget = new QWidget();
            labelWidget->setLayout(labelBox);
            QScrollArea *tArea = new QScrollArea();
            tArea->setWidget(labelWidget);
            tArea->setWidgetResizable(true);
            vBox->addWidget(tArea);


            QWidget *tWidget_2 = new QWidget();
            tWidget_2->setLayout(vBox);
            tabWidget->addTab(tWidget_2,"Gallery Info");
        }

        layout->addWidget(tabWidget);

        saveButton = new QPushButton("Save");
        saveButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        connect(saveButton,SIGNAL(clicked()),this,SLOT(onSaveClick()));
        layout->addWidget(saveButton);
        layout->setAlignment(saveButton, Qt::AlignRight);
        this->setLayout(layout);
        this->setWindowTitle("Person Edit");
    }

    curImage = 0;
    indexer = &Indexer::getInstance();
    path = indexer->path;
}

PersonEdit::~PersonEdit()
{
    delete ui;
}


///  ==================== User Interface =================================

void PersonEdit::closeEvent(QCloseEvent *)
{
//    if(!ifSaved){
//        QMessageBox::StandardButton personMessage;
//        personMessage = QMessageBox::question(this, "Person edit", "Save changes?",
//                                        QMessageBox::Yes|QMessageBox::No);
//          if(personMessage == QMessageBox::Yes)
//            onSaveClick();
//    }
}

void PersonEdit::clearSelection()
{
    curImage = -1;
    iLabel->clear();
    fileName->setText("");
    imageSize->setText("");
    lastModified->setText("");
}

void PersonEdit::onImageClick(int ID)
{
    QPixmap pixmap;
    Mat img;
    if(curImage != -1){
        img = gallery.getImage(curImage);
        if(ifEqualize) img = equalize(img);
        pixmap = QPixmap::fromImage(toQImage(img));
        imageLabels[curImage]->setPixmap(pixmap.scaled(QSize(width, height),Qt::IgnoreAspectRatio, Qt::FastTransformation));
    }
    img = gallery.getImage(ID);
    if(ifEqualize) img = equalize(img);
    pixmap = QPixmap::fromImage(toQImage(img));
//    iLabel->setPixmap(pixmap);
    iLabel->setPixmap(pixmap.scaled(QSize(150, 150),Qt::IgnoreAspectRatio, Qt::FastTransformation));

    fileName->setText(imageList[ID].fileName());
    if(!pixmap.isNull()){
        QString iSize = QString("%1 x %2").arg(pixmap.size().width()).arg(pixmap.size().height());
        imageSize->setText(iSize);
    }
    QDateTime dateTime = imageList[ID].lastModified();
    lastModified->setText(dateTime.toString("yyyy.MM.dd hh:mm"));

    pixmap = pixmap.scaled(QSize(width, height),Qt::IgnoreAspectRatio, Qt::FastTransformation);
    QPainter p(&pixmap);
    p.setPen(Qt::green);
    p.drawRect(QRect(0,0,width-1,height-1));
    p.end();
    imageLabels[ID]->setPixmap(pixmap);

    curImage = ID;
}

void PersonEdit::onLabelClick(int ID)
{
    galleryLabels[ID]->hide();
    gallery.DBLabels[ID].ifDelete = true;
}

void PersonEdit::onDeleteClick()
{
    if(curImage == -1)
        return;

    imageLabels[curImage]->hide();
    imageLabels[curImage]->deleteLater();
    imageLabels[curImage] = NULL;
    emit clearSelection();
}

void PersonEdit::onSaveClick()
{
    bool ifRename = false;

    if(personEdit->currentText() == ""){
        message.setText("Person name is empty");
        message.show();
        tabWidget->setCurrentIndex(1);
        personEdit->setFocus();
        return;
    }
    else{
        if(gallery.person.name != personEdit->currentText()){
            DBPerson tPerson;
            tPerson.name = personEdit->currentText();
            if(tPerson.getID() == -1){
                QMessageBox::StandardButton personMessage;
                personMessage = QMessageBox::question(this, "Person edit", "Create new person?",
                                                QMessageBox::Yes|QMessageBox::No);
                  if(personMessage == QMessageBox::Yes){
                      tPerson.save();
                      gallery.person = tPerson;
                  }
                  else{
                      ifRename = true;
                      gallery.person.name = personEdit->currentText();
                  }
            }
            else gallery.person = tPerson;
        }
    }

    if(QDateTime::fromString(dateEdit->text(), "yyyy.MM.dd_hh.mm").date().year() == 0){
        message.setText("Incorrect DateTime\nFormat is:  yyyy.MM.dd_hh.mm");
        message.show();
        tabWidget->setCurrentIndex(1);
        dateEdit->setFocus();
        return;
    }
    else gallery.dateTime = QDateTime::fromString(dateEdit->text(), "yyyy.MM.dd_hh.mm");

    gallery.name = gallery.person.name+" / "+gallery.dateTime.toString("dd MMM hh")+". stunda";

    for(QList<ClickLabel*>::iterator it = imageLabels.begin(); it < imageLabels.end(); ++it){
        if(*it == NULL){
            gallery.DBImages[it-imageLabels.begin()].ifDelete = true;
        }
    }

    ifSaved = gallery.saveAll(ifRename);
    emit closeEdit();
}

void PersonEdit::onAddClick()
{
    if(labelEdit->currentText() == ""){
        message.setText("Label name is empty");
        message.show();
        tabWidget->setCurrentIndex(1);
        labelEdit->setFocus();
        return;
    }

    for(QList<DBGalleryLabel>::iterator it = gallery.DBLabels.begin(); it < gallery.DBLabels.end(); ++it){
        if(!(*it).ifDelete && labelEdit->currentText() == (*it).label.name){
            message.setText("Label already exists");
            message.show();
            tabWidget->setCurrentIndex(1);
            labelEdit->setFocus();
            return;
        }
    }

    DBGalleryLabel tGLabel;
    tGLabel.label.name = labelEdit->currentText();
    tGLabel.gallery_id = gallery.getID();
    gallery.DBLabels.append(tGLabel);

    ClickLabel *clickLabel = new ClickLabel(this, galleryLabels.size());
    connect(clickLabel,SIGNAL(clicked(int)),this,SLOT(onLabelClick(int)));
    clickLabel->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    clickLabel->setTextPixmap(labelEdit->currentText());
    galleryLabels.append(clickLabel);
    labelBox->addWidget(clickLabel);

    if(labelEdit->findText(labelEdit->currentText()) == -1)
        labelEdit->addItem(labelEdit->currentText());
    labelEdit->setCurrentText("");
}

///  ==================== Load Gallery ===================================

void PersonEdit::loadImages(qint64 curGallery)
{ 
    // =====  Clear GUI  =========
    for(QList<ClickLabel*>::iterator it = imageLabels.begin(); it < imageLabels.end(); ++it){
        if((*it) != NULL){
            (*it)->hide();
            (*it)->deleteLater();
        }
    }
    imageLabels.clear();
    imageList.clear();
    emit clearSelection();

    for(QList<ClickLabel*>::iterator it = galleryLabels.begin(); it < galleryLabels.end(); ++it){
        (*it)->hide();
        (*it)->deleteLater();
    }

    galleryLabels.clear();
    dateEdit->clear();
    ifSaved = false;

    // =====  Take settings  =====
    width = settings.value("Width").toInt();
    height = settings.value("Height").toInt();
    ifEqualize = settings.value("Equalize").toBool();


    // =====  Load gallery  ======
    if(!gallery.loadAll(curGallery))
        return;
    dateEdit->setText(gallery.dateTime.toString("yyyy.MM.dd_hh.mm"));

    // =====  Load persons  ======
    personEdit->clear();
    personEdit->addItems(DBPerson::getStringList());
    personEdit->setCurrentIndex(personEdit->findText(gallery.person.name));

    // =====  Load images  =======
    int ID = 0;
    for(QList<DBImage>::iterator it = gallery.DBImages.begin(); it < gallery.DBImages.end(); ++it){
        if((*it).ifDelete)
            continue;
        Mat img = gallery.getImage(it-gallery.DBImages.begin());
        imageList.append((*it).filePath+"/"+(*it).fileName);

        ClickLabel *clickLabel = new ClickLabel(this,ID);
        connect(clickLabel,SIGNAL(clicked(int)),this,SLOT(onImageClick(int)));
        clickLabel->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        if(ifEqualize) img = equalize(img);
        QPixmap pixmap = QPixmap::fromImage(toQImage(img));
        pixmap = pixmap.scaled(QSize(width, height),Qt::IgnoreAspectRatio, Qt::FastTransformation);
        clickLabel->setPixmap(pixmap);
        imageLabels.append(clickLabel);
        imageBox->addWidget(clickLabel);
        ID++;
    }

    // =====  Load labels ========
    labelEdit->clear();
    labelEdit->addItems(DBLabel::getStringList());
    for(QList<DBGalleryLabel>::iterator it = gallery.DBLabels.begin(); it < gallery.DBLabels.end(); ++it){
        ClickLabel *clickLabel = new ClickLabel(this, galleryLabels.size());
        connect(clickLabel,SIGNAL(clicked(int)),this,SLOT(onLabelClick(int)));
        clickLabel->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        clickLabel->setTextPixmap((*it).label.name);
        galleryLabels.append(clickLabel);
        labelBox->addWidget(clickLabel);
    }
    labelEdit->setCurrentText("");

    this->curGallery = curGallery;
    this->tabWidget->setCurrentIndex(0);
}

///  ==================== Functions ======================================

QImage PersonEdit::toQImage(Mat inMat)
{
    switch(inMat.type()){
        case CV_8UC4:{
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );
            return image;
        }
        case CV_8UC3:{
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );
            return image.rgbSwapped();
        }
        case CV_8UC1:{
            static QVector<QRgb> sColorTable;
            if (sColorTable.isEmpty()){
               for (int i = 0; i < 256; ++i)
                  sColorTable.push_back( qRgb( i, i, i ) );
            }
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );
            image.setColorTable( sColorTable );
            return image;
        }
        default:
            qWarning() << "Mat image type not handled in switch:" << inMat.type();
        break;
    }

    return QImage();
}

Mat PersonEdit::equalize(Mat inMat)
{
    Mat dst;
    Mat src;
    cvtColor( inMat, src, CV_BGR2GRAY );
    Ptr<CLAHE> clahe = createCLAHE(10.0,Size(10,10));
    clahe->apply(src,dst);

    for(int i=0;i<dst.cols;i++){
        for(int j=0;j<dst.rows;j++){
            double num  = (1.0+((dst.at<uchar>(Point(i,j))-128.0)/256.0))*dst.at<uchar>(Point(i,j));
            num = dst.at<uchar>(Point(i,j)) - num;
            int hope = (int)(num*DELTA_PRIMARY);
            dst.at<uchar>(Point(i,j)) += hope;
        }
    }
    return dst;
}
