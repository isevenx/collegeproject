#ifndef PERSONCAPTURE_H
#define PERSONCAPTURE_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QSpacerItem>

#include <QDebug>
#include <QTime>
#include <QTimer>
#include <QImage>
#include <QPixmap>
#include <QThread>
#include <QDir>
#include <QDate>

#include <opencv2/opencv.hpp>

#include "grabber.h"
#include "classifier.h"
#include "recorder.h"

using namespace std;
using namespace cv;

namespace Ui {
class PersonCapture;
}

class PersonCapture : public QWidget
{
    Q_OBJECT

public:
    explicit PersonCapture(QWidget *parent = 0);
    ~PersonCapture();
signals:
    void sendFrame(Mat frame);
    void writeFace(Mat face);

public slots:
    void startGrab();
    void stopGrab();
    void onRecordClick();

private:
    Ui::PersonCapture *ui;
    QVBoxLayout *vBox;
    QSpacerItem *spacerItem;
    QPushButton *startButton, *stopButton, *recordButton;
    QGraphicsScene *scene;
    QGraphicsView *graphicsView;

    QThread *faceThread, *grabThread;
    Classifier *classifier;
    Grabber *grabber;
    Recorder *recorder;

    QTime bench;
    bool ifRecord, ifBusy, ifNew;
    int currentFrame;
    //int faceId;
    QString path;

    QImage toQImage(Mat inMat);

    void closeEvent(QCloseEvent*);
    void showEvent(QShowEvent*);

private slots:

    void takeFrame(Mat frame, int frameNr);
    void drawFrame(Mat frame, vector<Rect> faceVec);
};

#endif // PERSONCAPTURE_H
