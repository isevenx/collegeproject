#include "personcapture.h"
#include "ui_personcapture.h"

PersonCapture::PersonCapture(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PersonCapture)
{
    ui->setupUi(this);

    //    Form Creation
    {
        QHBoxLayout *hBox = new QHBoxLayout();
        vBox = new QVBoxLayout();
        startButton = new QPushButton("▶");  // ▷
        stopButton = new QPushButton("■");   // □
        recordButton = new QPushButton("○"); // ●
        //spacerItem = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
        scene = new QGraphicsScene();
        graphicsView = new QGraphicsView();

        vBox->addWidget(graphicsView);

        hBox->addStretch();
        hBox->addWidget(startButton);
        hBox->addWidget(stopButton);
        hBox->addWidget(recordButton);
        hBox->addStretch();

        vBox->addLayout(hBox);
        startButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        stopButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        recordButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
        this->setLayout(vBox);
        this->setWindowTitle("Person Capture");

        connect(startButton,SIGNAL(clicked()),this,SLOT(startGrab()));
        connect(stopButton,SIGNAL(clicked()),this,SLOT(stopGrab()));
        startButton->setEnabled(true);
        stopButton->setEnabled(false);

        graphicsView->setScene(scene);
    }

    //    Frame grabber class
    grabThread = new QThread();
    grabber = new Grabber();
    grabber->moveToThread(grabThread);
    grabThread->start();
    connect(grabber,SIGNAL(sendFrame(Mat,int)),this,SLOT(takeFrame(Mat,int)));

    //    Face detector class
    faceThread = new QThread();
    classifier = new Classifier();
    classifier->moveToThread(faceThread);
    faceThread->start();
    connect(this,SIGNAL(sendFrame(Mat)),classifier,SLOT(detectFaces(Mat)));
    connect(classifier,SIGNAL(sendFaces(Mat,vector<Rect>)),this,SLOT(drawFrame(Mat,vector<Rect>)));

    //    Recorder class
    recorder = new Recorder();
    connect(recordButton,SIGNAL(clicked()),this,SLOT(onRecordClick()));
    connect(this,SIGNAL(writeFace(Mat)),recorder,SLOT(addImage(Mat)));

    currentFrame = 0;
    ifRecord = false;
    ifBusy = false;
}

PersonCapture::~PersonCapture()
{
    delete ui;
}

///  ==================== User Interface =================================

void PersonCapture::closeEvent(QCloseEvent*)
{
    emit stopButton->click();
    if(ifNew == false) emit recorder->saveGallery();
    currentFrame = 0;
}

void PersonCapture::showEvent(QShowEvent*)
{
    ifNew = true;
    scene->clear();
}

void PersonCapture::startGrab()
{
    grabber->startGrab();
    startButton->setEnabled(false);
    stopButton->setEnabled(true);
}

void PersonCapture::stopGrab()
{
    grabber->stopGrab();
    startButton->setEnabled(true);
    stopButton->setEnabled(false);
//    if(recordButton->text() == "●"){
//        emit recordButton->click();
//    }
}

void PersonCapture::onRecordClick()
{
    if(recordButton->text() == "○"){
        if(ifNew){
            recorder->newRecord();
            ifNew = false;
        }
        ifRecord = true;
        recordButton->setText("●");
    }
    else{
        ifRecord = false;
        recordButton->setText("○");
    }
}

void PersonCapture::takeFrame(Mat frame, int frameNr)
{
    if(!ifBusy){
        emit sendFrame(frame.clone());
        ifBusy = true;
    }
    this->currentFrame = frameNr;
}

void PersonCapture::drawFrame(Mat frame, vector<Rect> faceVec)
{
    Mat face,frameCopy = frame.clone();

    for(int i = 0; i < int(faceVec.size()); i++){
        if(ifRecord){
            if(faceVec[i].width > face.cols){
                face = frame(faceVec[i]);
            }
            rectangle(frameCopy,faceVec[i],CV_RGB(255,0,0),2);
        }
        else rectangle(frameCopy,faceVec[i],CV_RGB(0,255,0),2);
    }

    if(ifRecord){
        if(!face.empty()){
            emit writeFace(face);
        }
    }

    QPixmap pixmap;
    pixmap = QPixmap::fromImage(toQImage(frameCopy));
    scene->clear();
    scene->addPixmap(pixmap);
    graphicsView->fitInView(pixmap.rect(),Qt::KeepAspectRatio);

    ifBusy = false;
}

///  ==================== Functions ======================================

QImage PersonCapture::toQImage(Mat inMat)
{
    switch ( inMat.type() )
    {
        // 8-bit, 4 channel
        case CV_8UC4:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );
            return image;
        }

        // 8-bit, 3 channel
        case CV_8UC3:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );
            return image.rgbSwapped();
        }

        // 8-bit, 1 channel
        case CV_8UC1:
        {
            static QVector<QRgb>  sColorTable;
            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
               for ( int i = 0; i < 256; ++i )
                  sColorTable.push_back( qRgb( i, i, i ) );
            }
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );
            image.setColorTable( sColorTable );
            return image;
        }

        default:
            qWarning() << "Mat image type not handled in switch:" << inMat.type();
        break;
    }
    return QImage();
}
