#include "imagesettings.h"
#include "ui_imagesettings.h"

ImageSettings::ImageSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ImageSettings)
{
    ui->setupUi(this);
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(takeSettings()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(close()));
    connect(ui->buttonBox,SIGNAL(rejected()),this,SLOT(close()));

    settings.setValue("Width", ui->lineEdit->text());
    settings.setValue("Height",ui->lineEdit_2->text());
    settings.setValue("Equalize",ui->checkBox->isChecked());
}

ImageSettings::~ImageSettings()
{
    delete ui;
}

void ImageSettings::closeEvent(QCloseEvent *)
{
    ui->lineEdit->setText(settings.value("Width").toString());
    ui->lineEdit_2->setText(settings.value("Height").toString());
    ui->checkBox->setChecked(settings.value("Equalize").toBool());
}

void ImageSettings::takeSettings()
{
    if(ui->lineEdit->text().toInt() == 0 || ui->lineEdit_2->text().toInt() == 0)
        return;
    settings.setValue("Width", ui->lineEdit->text());
    settings.setValue("Height",ui->lineEdit_2->text());
    settings.setValue("Equalize",ui->checkBox->isChecked());
    emit applySettings();
}
