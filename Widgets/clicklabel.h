#ifndef ClickLabel_H
#define ClickLabel_H

#include <QLabel>
#include <QObject>
#include <QPainter>
#include <QPixmap>
#include <QColor>
#include <QPen>
#include <QDebug>

class ClickLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ClickLabel(QWidget *parent = 0, const int &ID = 0);
    ~ClickLabel();

    void setTextPixmap(QString text);

signals:
    void clicked(int ID);

public slots:
    void click();

private:
    int ID;
    void mousePressEvent(QMouseEvent *ev);
};

#endif // ClickLabel_H
