#include "gallerywindow.h"
#include <QApplication>
#include <QMetaType>
#include "personwindow.h"

#include <opencv2/opencv.hpp>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GalleryWindow w;
    w.show();

    qRegisterMetaType< cv::Mat >("Mat");
    qRegisterMetaType< std::vector<cv::Rect> >("vector<Rect>");

    return a.exec();
}
