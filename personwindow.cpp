#include "personwindow.h"
#include "ui_personwindow.h"

PersonWindow::PersonWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PersonWindow)
{
    ui->setupUi(this);

    connect(ui->stackedWidget,SIGNAL(currentChanged(int)),this,SLOT(onWidgetChange(int)));
    connect(ui->page_2,SIGNAL(closeEdit()),this,SLOT(close()));

    indexer = &Indexer::getInstance();
    imageSettings = new ImageSettings();
    connect(ui->actionSettings,SIGNAL(triggered()),imageSettings,SLOT(show()));
    connect(imageSettings,SIGNAL(applySettings()),ui->page_2,SLOT(reloadImages()));
}

PersonWindow::~PersonWindow()
{
    delete ui;
}

///  ==================== User Interface =================================

void PersonWindow::closeEvent(QCloseEvent *)
{
    ui->stackedWidget->currentWidget()->close();
    emit updateInfo();
}

void PersonWindow::showCapture()
{
    this->setWindowTitle("Person Capture");
    ui->stackedWidget->setCurrentWidget(ui->page);
    ui->menubar->hide();
    ui->page->show();
    this->show();
}

void PersonWindow::showEdit(qint64 dbID)
{
    this->setWindowTitle("Person Edit");
    ui->stackedWidget->setCurrentWidget(ui->page_2);
//    imageSettings->takeSettings();
    ui->page_2->loadImages(dbID);
    ui->menubar->show();
    ui->page_2->show();
    this->show();
}

void PersonWindow::onWidgetChange(int ID)
{
    if(ID == 0){
        ui->page->stopGrab();
    }
}
