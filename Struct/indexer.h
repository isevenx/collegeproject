#ifndef INDEXER_H
#define INDEXER_H

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfoList>
#include <QStringList>
#include <QtAlgorithms>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDateTime>
#include <QList>

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QProgressBar>
#include <QMessageBox>

#include "dbgallery.h"

using namespace std;

class Indexer
{

public:
    static Indexer& getInstance()
    {
        static Indexer instance;
        return instance;
    }
    ~Indexer();

    QString path;

    QList<DBGallery> DBGalleries;
    QList<DBGallery::GalleryInfo> galleryInfo;

    void quickScan();
    void deepScan();

    bool checkExist(QString personName, QDateTime dateTime);

private:
    Indexer();
    Indexer(Indexer const& copy);
    Indexer& operator=(Indexer const& copy);

    QSqlDatabase db;
    QMessageBox message;
};

#endif // INDEXER_H
