#ifndef DBIMAGE_H
#define DBIMAGE_H

#include <QObject>
#include <QDebug>
#include <QDir>
#include <QDateTime>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include <opencv2/opencv.hpp>

#include "dbperson.h"

using namespace cv;

class DBImage
{
public:
    DBImage();
    ~DBImage();
    bool save(QString personName, QDateTime dateTime);
    bool load(qint64 dbID);
    static QList<DBImage> getList(qint64 galleryID);

    void setMat(Mat img);
    Mat getMat(QString personName, QDateTime dateTime);

    QString getPath();
    inline qint64 getID(){return dbID;}

    int gallery_id;
    QString fileName, filePath;
    bool ifDelete, ifWrite, ifChanged;

private:
    Mat img;
    qint64 dbID;
};

#endif // DBIMAGE_H
