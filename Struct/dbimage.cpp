#include "dbimage.h"

DBImage::DBImage()
{
    this->dbID = -1;
    this->ifDelete = false;
    this->ifWrite = false;
    this->ifChanged = false;
    this->fileName = "";
}

DBImage::~DBImage()
{

}

bool DBImage::save(QString personName, QDateTime dateTime)
{
    QSqlQuery query;
    filePath = QDir::homePath()+"/Pictures/Person_Images" + "/"+\
            personName + "/" + dateTime.toString("yyyy.MM.dd_hh.mm");

    if(ifDelete && dbID != -1){
        query.prepare("DELETE FROM Images WHERE id = :id");
        query.bindValue(":id",dbID);
        if(query.exec()){
            QFile file(filePath+"/"+fileName);
            file.remove();
            return true;
        }
        else return false;
    }

    if(ifWrite){
        if(dbID == -1){
            query.prepare("INSERT INTO Images(gallery_id,filename) VALUES(:gallery_id,:filename)");
            query.bindValue(":gallery_id", gallery_id);
            query.bindValue(":filename",fileName);
            if(query.exec())
                dbID = query.lastInsertId().toInt();
            else return false;
        }

        QDir dir(filePath);
        if(!dir.exists())
            dir.mkpath(filePath);
        fileName = QString("%1_%2.jpg").arg(dbID,4,10,QLatin1Char('0')).arg(dateTime.toMSecsSinceEpoch());
//        fileName = QString::number(dbID)+"_"+QString::number(dateTime.toMSecsSinceEpoch())+".jpg";
        imwrite(QString(filePath+"/"+fileName).toStdString(), img);
    }

    if(dbID == -1){
        query.prepare("INSERT INTO Images(gallery_id,filename) VALUES(:gallery_id,:filename)");
    }
    else{
        query.prepare("UPDATE Images SET "
                      "gallery_id = :gallery_id, "
                      "filename = :filename "
                      "WHERE id = :id");
        query.bindValue(":id",dbID);
    }
    query.bindValue(":gallery_id", gallery_id);
    query.bindValue(":filename",fileName);

    if(!query.exec())
        return false;
    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DBImage::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Images WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->gallery_id = query.value("gallery_id").toInt();
        this->fileName = query.value("filename").toString();
        //this->filePath = getPath();
        this->dbID = dbID;
        return true;
    }
    else return false;
}

QList<DBImage> DBImage::getList(qint64 galleryID)
{
    QList<DBImage> result;
    QSqlQuery query;
    query.prepare("SELECT * FROM Images WHERE gallery_id = :gallery_id");
    query.bindValue(":gallery_id", galleryID);
    if(!query.exec()) return result;

    while(query.next()){
        DBImage tImage;
        tImage.gallery_id = query.value("gallery_id").toInt();
        tImage.fileName = query.value("filename").toString();
        tImage.dbID = query.value("id").toInt();
        result.append(tImage);
    }
    return result;
}

void DBImage::setMat(Mat img)
{
    this->img = img;
    this->ifWrite = true;
}

Mat DBImage::getMat(QString personName, QDateTime dateTime)
{
    if(img.empty()){
        filePath = QDir::homePath()+"/Pictures/Person_Images" + "/"+\
                personName + "/" + dateTime.toString("yyyy.MM.dd_hh.mm");

        img = imread(QString(filePath+"/"+fileName).toStdString());
        if(img.empty())
            img = Mat(100, 100, CV_8UC3, Scalar(255, 255, 255));
    }

    return img;
}

QString DBImage::getPath()
{
    int person_id = -1;
    QString path, person, datetime;
    path = QDir::homePath()+"/Pictures/Person_Images";

    QSqlQuery query;
    query.prepare("SELECT * FROM Galleries WHERE id = :id");
    query.bindValue(":id", gallery_id);
    if(query.exec() && query.first()){
        person_id = query.value("person_id").toInt();
        datetime = query.value("datetime").toDateTime().toString("yyyy.MM.dd_hh.mm");
    }
    else path = "failed";

    query.prepare("SELECT * FROM Persons WHERE id = :id");
    query.bindValue(":id", person_id);
    if(query.exec() && query.first()){
        person = query.value("name").toString();
    }
    else path = "failed";

    path +="/"+person+"/"+datetime;
    return path;

}
