#ifndef DBGALLERY_H
#define DBGALLERY_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QFile>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>

#include <QJsonValue>
#include <QJsonArray>
#include <QJsonValueRef>
#include <QJsonObject>
#include <QJsonDocument>

#include "dbgallerylabel.h"
#include "dbimage.h"
#include "dbperson.h"

using namespace std;

class DBGallery
{
public:
    DBGallery();
    ~DBGallery();

    typedef struct{
        qint64 galleryID;
        QString galleryName;
        QDateTime galleryDate;
        QString personName;
        qint64 imageCount;
    } GalleryInfo;


    bool save();
    bool saveAll(bool ifRename = false);
    bool load(qint64 dbID);
    bool loadAll(qint64 dbID);
    static QList<GalleryInfo> getGalleryInfo();
    static QList<DBGallery> getList();
    bool deleteAll();

    void addImage(DBImage &image);
    Mat getImage(int imgID);

    static QStringList readJson(QString path);
    bool writeJson(QString path, QStringList labels);

    void checkFiles();

    QString name;
    QDateTime dateTime;
    bool ifDelete;

    DBPerson person;
    QList<DBImage> DBImages;
    QList<DBGalleryLabel> DBLabels;

    inline qint64 getID(){return dbID;}

private:
    qint64 dbID;
    QString getPerson();
    QString getDateTime();
};

#endif // DBGALLERY_H
