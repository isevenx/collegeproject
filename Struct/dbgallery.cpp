#include "dbgallery.h"

DBGallery::DBGallery()
{
    this->dbID = -1;
}

DBGallery::~DBGallery()
{

}

///  ==================== Save Gallery ===================================

bool DBGallery::save()
{
    person.save();

    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO Galleries(name,datetime,person_id)"
                      "VALUES(:name,:datetime,:person_id)");
    }
    else{
        query.prepare("UPDATE Galleries SET "
                      "name = :name, "
                      "datetime = :datetime, "
                      "person_id = :person_id "
                      "WHERE id = :id");
        query.bindValue(":id", this->dbID);
    }
    query.bindValue(":name",this->name);
    query.bindValue(":datetime",this->dateTime);
    query.bindValue(":person_id",this->person.getID());
    if(!query.exec()){
        qDebug() << query.lastError().text();
        return false;
    }

    if(dbID == -1) dbID = query.lastInsertId().toInt();
    return true;
}

bool DBGallery::saveAll(bool ifRename)
{
    // ===== Person save =====
    if(!person.save())
        return false;

    // ===== Folder save =====
    QString oldPath = QDir::homePath()+"/Pictures/Person_Images/"+getPerson()+"/"+getDateTime();
    QString newPath = QDir::homePath()+"/Pictures/Person_Images/"+person.name+"/"+dateTime.toString("yyyy.MM.dd_hh.mm");
    if(oldPath != newPath && !oldPath.contains("failed")){
        QDir dir;
        if(ifRename){
            oldPath.chop(oldPath.length()-oldPath.lastIndexOf("/"));
            newPath.chop(newPath.length()-newPath.lastIndexOf("/"));
        }
        else{
            dir.mkpath(QDir::homePath()+"/Pictures/Person_Images/"+person.name);
        }
        if(!dir.rename(oldPath,newPath)){
            qDebug() << "MOVE FAILED"<<oldPath<<newPath;
            return false;
        }
    }

    // ===== Image save ======
    for(QList<DBImage>::iterator it = DBImages.begin(); it < DBImages.end(); it++){
        if((*it).ifDelete || (*it).ifWrite || (*it).ifChanged){
            if(!(*it).save(person.name, dateTime))
                return false;
        }
        (*it).ifChanged = false;
    }

    // ===== Label save ======
    QStringList labelList;
    for(QList<DBGalleryLabel>::iterator it = DBLabels.begin(); it < DBLabels.end(); ++it){
        if(!(*it).save())
            return false;
        if(!(*it).ifDelete)
            labelList.append((*it).label.name);
    }
    newPath = QDir::homePath()+"/Pictures/Person_Images/"+person.name+"/"+dateTime.toString("yyyy.MM.dd_hh.mm");
    writeJson(newPath, labelList);

    // ===== Gallery save ====
    QSqlQuery query;
    if(dbID == -1){
        query.prepare("INSERT INTO Galleries(name,datetime,person_id)"
                      "VALUES(:name,:datetime,:person_id)");
    }
    else{
        query.prepare("UPDATE Galleries SET "
                      "name = :name, "
                      "datetime = :datetime, "
                      "person_id = :person_id "
                      "WHERE id = :id");
        query.bindValue(":id", this->dbID);
    }
    query.bindValue(":name",this->name);
    query.bindValue(":datetime",this->dateTime);
    query.bindValue(":person_id",this->person.getID());
    if(!query.exec()){
        qDebug() << query.lastError().text();
        return false;
    }

    if(dbID == -1) dbID = query.lastInsertId().toInt();

    qDebug() << this->name;
    return true;
}

///  ==================== Load Gallery ===================================

bool DBGallery::load(qint64 dbID)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM Galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->name = query.value("name").toString();
        this->dateTime = query.value("datetime").toDateTime();
        this->person.load(query.value("person_id").toInt());
        this->dbID = dbID;

        return true;
    }
    return false;
}

bool DBGallery::loadAll(qint64 dbID)
{
    bool ok = true;
    QSqlQuery query;
    query.prepare("SELECT * FROM Galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        this->name = query.value("name").toString();
        this->dateTime = query.value("datetime").toDateTime();
        this->dbID = dbID;
    }
    else ok = false;

    if(ok){
        DBImages.clear();
        DBImages = DBImage::getList(this->dbID);
        DBLabels = DBGalleryLabel::getList(this->dbID);
        person.load(query.value("person_id").toInt());
    }

    return ok;
}

///  ==================== Get Gallery ====================================

QList<DBGallery::GalleryInfo> DBGallery::getGalleryInfo()
{
    QList<GalleryInfo> result;
    QSqlQuery query;
    query.prepare("SELECT g.id AS id, g.name AS name, g.datetime AS date, COUNT(i.id) AS icount, p.name AS pname "
                  "FROM Galleries g, Images i, Persons p WHERE i.gallery_id = g.id AND p.id = g.person_id GROUP BY g.id");
    if(!query.exec()) return result;

    while(query.next()){
        GalleryInfo tInfo;
        tInfo.galleryID = query.value("id").toInt();
        tInfo.galleryName = query.value("name").toString();
        tInfo.galleryDate = query.value("date").toDateTime();
        tInfo.personName = query.value("pname").toString();
        tInfo.imageCount = query.value("icount").toInt();
        result.append(tInfo);
    }
    return result;
}

QList<DBGallery> DBGallery::getList()
{
    QList<DBGallery> result;
    QSqlQuery query;
    query.prepare("SELECT * FROM Galleries");
    if(!query.exec()) return result;
    while(query.next()){
        DBGallery tGallery;
        tGallery.load(query.value("id").toInt());
        result.append(tGallery);
    }
    return result;
}

///  ==================== Add/Get Image ==================================

void DBGallery::addImage(DBImage &image)
{
    image.gallery_id = this->dbID;
    DBImages.append(image);
}

Mat DBGallery::getImage(int imgID)
{
    if(imgID < 0 || imgID > DBImages.size()-1)
        return Mat(100, 100, CV_8UC3, Scalar(255, 255, 255));
    else return DBImages[imgID].getMat(person.name, dateTime);
}

///  ==================== Delete Gallery =================================

bool DBGallery::deleteAll()
{
    if(dbID != -1){

        QString path;
        path = QDir::homePath()+"/Pictures/Person_Images" + "/"+\
                person.name + "/" + dateTime.toString("yyyy.MM.dd_hh.mm");
        QDir dir(path);
        if(dir.exists()){
            if(!dir.removeRecursively())
                return false;
        }

        QSqlQuery query;
        query.prepare("DELETE FROM Images WHERE gallery_id = :id");
        query.bindValue(":id", dbID);
        if(!query.exec()) return false;

        query.prepare("DELETE FROM Galleries WHERE id = :id");
        query.bindValue(":id", dbID);
        if(!query.exec()) return false;

        query.prepare("DELETE FROM GalleryLabels WHERE gallery_id = :id");
        query.bindValue(":id", dbID);
        if(!query.exec()) return false;

        qDebug() << "Gallery" << name << "deleted";
        return true;
    }
    else return false;
}

///  ==================== Check Gallery ==================================

void DBGallery::checkFiles()
{
    if(DBImages.empty())
        DBImages = DBImage::getList(this->dbID);
    if(DBLabels.empty())
        DBLabels = DBGalleryLabel::getList(this->dbID);

    QString path;
    path = QDir::homePath()+"/Pictures/Person_Images" + "/"+\
            person.name + "/" + dateTime.toString("yyyy.MM.dd_hh.mm");
    QDir dir(path);

    // ===== Checking folder =====

    if(!dir.exists()){
        deleteAll();
        return;
    }

    // ===== Checking images =====

    dir.setSorting(QDir::LocaleAware);
    QStringList imageList = dir.entryList(QStringList("*.jpg"), QDir::Files|QDir::NoDotAndDotDot);

    for(QList<DBImage>::iterator it = DBImages.begin(); it < DBImages.end(); ++it){
        (*it).ifDelete = true;
        for(QStringList::iterator iit = imageList.begin(); iit < imageList.end(); ++iit){
            if((*it).fileName == (*iit)){
                (*it).ifDelete = false;
                imageList.removeAt(iit-imageList.begin());
                break;
            }
        }
        if((*it).ifDelete == true){
            (*it).save(person.name, dateTime);
            qDebug() << "Image " << (*it).fileName << " deleted";
        }
    }

    for(QStringList::iterator it = imageList.begin(); it < imageList.end(); ++it){
        DBImage tImage;
        tImage.fileName = (*it);
        tImage.gallery_id = this->dbID;
        tImage.save(person.name, dateTime);
        DBImages.append(tImage);
        qDebug() << "Image " << (*it) << " added";
    }

    // ===== Checking labels =====

    QStringList labelList = readJson(path);
    for(QList<DBGalleryLabel>::iterator it = DBLabels.begin(); it < DBLabels.end(); ++it){
        (*it).ifDelete = true;
        for(QStringList::iterator iit = labelList.begin(); iit < labelList.end(); ++iit){
            if((*it).label.name == (*iit)){
                (*it).ifDelete = false;
                labelList.removeAt(iit-labelList.begin());
                break;
            }
        }
        if((*it).ifDelete == true){
            (*it).save();
            qDebug() << "Label " << (*it).label.name << " deleted";
        }
    }

    for(QStringList::iterator it = labelList.begin(); it < labelList.end(); ++it){
        DBGalleryLabel tGLabel;
        tGLabel.label.name = (*it);
        tGLabel.gallery_id = this->dbID;
        tGLabel.save();
        DBLabels.append(tGLabel);
        qDebug() << "Label " << (*it) << " added";
    }
}

///  ==================== Functions ======================================

QString DBGallery::getPerson()
{
    QString person;
    QSqlQuery query;
    int person_id;

    query.prepare("SELECT * FROM Galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        person_id = query.value("person_id").toInt();
    }
    else return "failed";

    query.prepare("SELECT * FROM Persons WHERE id = :id");
    query.bindValue(":id", person_id);
    if(query.exec() && query.first()){
        person = query.value("name").toString();
    }
    else person = "failed";

    return person;
}

QString DBGallery::getDateTime()
{
    QDateTime dateTime;
    QSqlQuery query;
    query.prepare("SELECT * FROM Galleries WHERE id = :id");
    query.bindValue(":id", dbID);
    if(query.exec() && query.first()){
        dateTime = query.value("datetime").toDateTime();
    }
    else return "failed";

    return dateTime.toString("yyyy.MM.dd_hh.mm");
}

QStringList DBGallery::readJson(QString path)
{
    QStringList labels;
    int tagCount = 0;
    QFile jFile(path+"/labels.json");
    if(jFile.open(QIODevice::ReadOnly)){

        QString jsonFile = jFile.readAll();
        QJsonDocument mainDoc = QJsonDocument::fromJson(jsonFile.toUtf8());
        QJsonObject mainObject;
        mainObject = mainDoc.object();

        while(mainObject.contains(QString("label%1").arg(tagCount))){
            labels.append(mainObject[QString("label%1").arg(tagCount)].toString());
            tagCount++;
        }
    }
    else{
        jFile.open(QIODevice::WriteOnly);
        QJsonObject mainObject;
        mainObject.insert("label0", QStringLiteral("None"));
        labels.append(mainObject["label0"].toString());
        QJsonDocument mainDocument = QJsonDocument(mainObject);
        jFile.write(mainDocument.toJson());
    }

    jFile.close();
    return labels;
}

bool DBGallery::writeJson(QString path, QStringList labels)
{
    QFile jFile(path+"/labels.json");
    if(jFile.open(QIODevice::WriteOnly)){
        QJsonObject mainObject;
        for(QStringList::iterator it = labels.begin(); it < labels.end(); ++it){
            QString tString = *it;
            mainObject.insert(QString("label%1").arg(it-labels.begin()),tString);
        }
        QJsonDocument mainDocument = QJsonDocument(mainObject);
        jFile.write(mainDocument.toJson());
        jFile.close();
        return true;
    }
    else return false;
}
