INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/indexer.h \
    $$PWD/dbgallery.h \
    $$PWD/dblabels.h \
    $$PWD/dblabel.h \
    $$PWD/dbimage.h \
    $$PWD/dbgallerylabel.h \
    $$PWD/dbperson.h

SOURCES += \
    $$PWD/indexer.cpp \
    $$PWD/dbgallery.cpp \
    $$PWD/dblabel.cpp \
    $$PWD/dbimage.cpp \
    $$PWD/dbgallerylabel.cpp \
    $$PWD/dbperson.cpp
