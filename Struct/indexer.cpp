#include "indexer.h"

Indexer::Indexer()
{
    path = QDir::homePath()+"/Pictures/Person_Images";
    db = QSqlDatabase::addDatabase("QSQLITE");

    QDir initial(path);
    if(!initial.mkpath(".")){
        initial = QDir::home();
    }

    db.setDatabaseName( initial.filePath("galleries.db"));
    bool ok = db.open();
    if(ok)
        qDebug() << "Hello";

    QSqlQuery q;

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS Persons(" \
                "id                 INTEGER, " \
                "name               TEXT UNIQUE, "\
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS Labels(" \
                "id                 INTEGER, " \
                "name               TEXT UNIQUE, "\
                "PRIMARY KEY(id ASC));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS Galleries(" \
                "id                 INTEGER, " \
                "name               TEXT DEFAULT \"\", "\
                "datetime           DATETIME, "\
                "person_id          INTEGER, " \
                "PRIMARY KEY(id ASC), "\
                "FOREIGN KEY(person_id) REFERENCES Persons(id));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS Images(" \
                "id                 INTEGER, " \
                "gallery_id         INTEGER, "\
                "filename           TEXT DEFAULT \"\", "\
                "PRIMARY KEY(id ASC), "\
                "FOREIGN KEY(gallery_id) REFERENCES Galleries(id));");

    if(!q.isActive()) qDebug() << q.lastError().text();

    q = db.exec(
                "CREATE TABLE IF NOT EXISTS GalleryLabels(" \
                "id                 INTEGER, " \
                "gallery_id         INTEGER, "\
                "label_id           INTEGER, "\
                "PRIMARY KEY(id ASC), "\
                "FOREIGN KEY(gallery_id) REFERENCES Galleries(id), "\
                "FOREIGN KEY(label_id) REFERENCES Labels(id));");

    if(!q.isActive()) qDebug() << q.lastError().text();

}

Indexer::~Indexer()
{

}

void Indexer::quickScan()
{
    galleryInfo = DBGallery::getGalleryInfo();

    qDebug() << "Checking for new galleries";
    int galleryCount = 0;

    QDir mainDir(path);
    QStringList personList = mainDir.entryList(QDir::Dirs|QDir::NoDotAndDotDot);
    for(QStringList::iterator it = personList.begin(); it < personList.end(); ++it){

        QDir dir(path+"/"+(*it));
        QStringList galleryList = dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot);

        for(QStringList::iterator iit = galleryList.begin(); iit < galleryList.end(); ++iit){

            QString personName = (*it);
            QDateTime dateTime = QDateTime::fromString((*iit), "yyyy.MM.dd_hh.mm");
            if(dateTime.date().year() != 0){
                if(!checkExist(personName, dateTime)){

                    //    ADD NEW GALLERY
                    DBGallery tGallery;
                    tGallery.person.name = personName;
                    tGallery.dateTime = dateTime;
                    tGallery.name = personName+" / "+tGallery.dateTime.toString("dd MMM hh")+". stunda";
                    tGallery.save();

                    //    ADD IMAGES
                    QDir dir(path+"/"+*it+"/"+*iit);
                    dir.setSorting(QDir::Name);
                    QStringList imageList = dir.entryList(QStringList("*.jpg"), QDir::Files|QDir::NoDotAndDotDot);
                    for(QStringList::iterator it = imageList.begin(); it < imageList.end(); ++it){
                        DBImage tImage;
                        tImage.fileName = (*it);
                        tImage.gallery_id = tGallery.getID();
                        tImage.ifChanged = true;
                        tGallery.DBImages.append(tImage);
                    }

                    if(imageList.empty())
                        continue;

                    //    READ LABELS
                    QStringList labelList = DBGallery::readJson(dir.absolutePath());
                    for(QStringList::iterator it = labelList.begin(); it < labelList.end(); ++it){
                        DBGalleryLabel tGLabel;
                        tGLabel.label.name = (*it);
                        tGLabel.gallery_id = tGallery.getID();
                        tGallery.DBLabels.append(tGLabel);
                    }

                    tGallery.saveAll();
                    galleryCount++;
                }
            }
        }
    }

    qDebug() << "New galleries found: " << galleryCount;
    qDebug() << "";
}

void Indexer::deepScan()
{
    qDebug() << "";
    qDebug() << "Checking existing galleries";

    DBGalleries = DBGallery::getList();
    for(QList<DBGallery>::iterator it = DBGalleries.begin(); it < DBGalleries.end(); ++it){
        (*it).checkFiles();
    }

    qDebug() << "Check complited";
    quickScan();
}

bool Indexer::checkExist(QString personName, QDateTime dateTime)
{
    if(galleryInfo.empty())
        galleryInfo = DBGallery::getGalleryInfo();

    for(QList<DBGallery::GalleryInfo>::iterator it = galleryInfo.begin(); it < galleryInfo.end(); ++it){
        if(personName == (*it).personName && dateTime == (*it).galleryDate)
            return true;
    }
    return false;
}
