#include "grabber.h"

Grabber::Grabber(QObject *parent) : QObject(parent)
{
    FPS = 30;
    currentFrame = 0;
    ifStop = true;
    cap = new VideoCapture();
}

Grabber::~Grabber()
{

}

void Grabber::startGrab()
{
    if(!cap->isOpened()){
        cap->open(0);
        if(!cap->isOpened()){
            qDebug() << "No connection with camera";
            return;
        }
    }
    ifStop = false;
    QTimer::singleShot(1,this,SLOT(grab()));
}

void Grabber::stopGrab()
{
    currentFrame = 0;
    ifStop = true;
    for(int i = 10; i > 1; i--)
        cap->grab();
}

void Grabber::grab()
{
    if(ifStop)
        return;

    int readtime;
    QTime readTime;
    readTime.start();

    Mat frame;
    if(cap->read(frame)){
        currentFrame++;
        emit sendFrame(frame,currentFrame);
    }

    readtime = readTime.elapsed();
    if(readtime > 1000/FPS){
        readtime = -(1000/FPS-1);
    }

    QTimer::singleShot(1000/FPS-readtime,this,SLOT(grab()));
}
