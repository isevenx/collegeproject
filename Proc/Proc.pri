INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/classifier.h \
    $$PWD/grabber.h \
    $$PWD/recorder.h

SOURCES += \
    $$PWD/classifier.cpp \
    $$PWD/grabber.cpp \
    $$PWD/recorder.cpp

