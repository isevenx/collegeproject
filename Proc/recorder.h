#ifndef RECORDER_H
#define RECORDER_H

#include <QObject>
#include <QTime>
#include <QDate>
#include <QDir>
#include <QDebug>

#include <opencv2/opencv.hpp>

#include "indexer.h"

using namespace cv;

class Recorder : public QObject
{
    Q_OBJECT
public:
    explicit Recorder(QObject *parent = 0);
    ~Recorder();

signals:
    void addRecord(DBGallery*);

public slots:
    void addImage(Mat img);
    inline void saveGallery(){ gallery.saveAll(); }
    void newRecord();

private:
    QDir dir;
    QTime time;
    QDate date;

    DBGallery gallery;
};

#endif // RECORDER_H
