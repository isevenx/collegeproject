#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <QObject>
#include <QThread>
#include <QTime>
#include <QDebug>
#include <QFileInfo>
#include <QDir>

#include <opencv2/opencv.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

class Classifier : public QObject
{
    Q_OBJECT
public:
    explicit Classifier(QObject *parent = 0);
    ~Classifier();

signals:
    void sendFaces(Mat frame,vector<Rect> faceVec);

public slots:
    void detectFaces(Mat frame);

private:
    CascadeClassifier *cascade;

};

#endif // CLASSIFIER_H
