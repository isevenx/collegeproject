#include "classifier.h"

Classifier::Classifier(QObject *parent) : QObject(parent)
{
    cascade = new CascadeClassifier();
    QFile file(":/Cascades/haarcascade_frontalface_alt.xml");
    if(!QFileInfo(QDir::current().absoluteFilePath("cascade.xml")).exists())
        file.copy(QDir::current().absoluteFilePath("cascade.xml"));
    cascade->load("cascade.xml");
//    cascade->load("cascade_dog.xml");
//    cascade->load("cascade_tank.xml");

}

Classifier::~Classifier()
{

}

void Classifier::detectFaces(Mat frame)
{
    vector<Rect> faceVec;

//    QTime bench;
//    bench.start();

    cascade->detectMultiScale(frame,faceVec,1.3,3,3|CV_HAAR_SCALE_IMAGE,Size(100,100));

//    qDebug() << bench.elapsed();

    emit sendFaces(frame, faceVec);
}
